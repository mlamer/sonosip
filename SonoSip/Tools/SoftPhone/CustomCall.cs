﻿namespace SonoSip.Tools.SoftPhone
{
    using pjsua2;

    class CustomCall : Call
    {
        SoftPhone ancestor;

        public CallState State { get; set; }
        public string ParticipantName { get; set; }
        public string AccessCode { get; set; }
        public int Id { get; set; }

        public AudioMedia audioMedia { get; set; }
        private AudioMediaPlayer audioMediaPlayer;

        public CustomCall(SoftPhone softPhone, Account account, int callId)
            : base(account, callId)
        {
            Id = callId;
            ancestor = softPhone;
            AccessCode = "";
            ParticipantName = "Participant non authentifié";
            State = CallState.Connecting;
        }

        public override void onCallState(OnCallStateParam param)
        {
            CallInfo callInfo = getInfo();
            if (callInfo.state == pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED)
            {
                foreach (CallMediaInfo mediaInfo in callInfo.media)
                {
                    if (mediaInfo.type == pjmedia_type.PJMEDIA_TYPE_AUDIO)
                    {
                        audioMedia = AudioMedia.typecastFromMedia(getMedia(mediaInfo.index));
                        break;
                    }
                }
            }

            ancestor.OnCallStateUpdated(this);
        }

        public override void onDtmfDigit(OnDtmfDigitParam param)
        {
            ancestor.OnCallDtmfDigit(this, param);
        }

        public void startPlayer(string soundToPlay)
        {
            stopPlayer();
            audioMediaPlayer = new AudioMediaPlayer();
            audioMediaPlayer.createPlayer(System.IO.Path.Combine(ancestor.resourcesPath, soundToPlay));
            audioMediaPlayer.startTransmit(audioMedia);
        }

        public void stopPlayer()
        {
            if (audioMediaPlayer != null)
            {
                audioMediaPlayer.Dispose();
                audioMediaPlayer = null;
            }
        }

        public override void Dispose()
        {
            stopPlayer();
            base.Dispose();
        }
    }
}
