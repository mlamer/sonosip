﻿namespace SonoSip.Tools.SoftPhone
{
    using pjsua2;
    using SonoSip.Models;
    using System;
    using System.Collections.Generic;

    class SoftPhone : IDisposable
    {
        const string DTMF_SHARP = "#";
        const string DTMF_STAR = "*";
        const string DTMF_REQUEST_TO_SPEAK = "7";
        const string DTMF_CANCEL_REQUEST_TO_SPEAK = "9";

        private static volatile SoftPhone instance;
        private static object syncRoot = new Object();

        private Endpoint endpoint;
        private CustomAccount account;
        public List<CustomCall> callList;
        public List<Participant> userList { get; set; }
        public string resourcesPath { get; set; }

        private string _idUri;
        private string _registrarUri;
        private string _realm;
        private string _username;
        private string _password;

        private bool _isDisposed = false;

        private ConnectionState connectionState;

        private SoftPhone()
        {
            connectionState = ConnectionState.Disconnected;
            callList = new List<CustomCall>();

            endpoint = new Endpoint();
            endpoint.libCreate();

            EpConfig epConfig = new EpConfig();
            epConfig.uaConfig.mainThreadOnly = false;
            epConfig.uaConfig.maxCalls = 30;

            endpoint.libInit(epConfig);

            TransportConfig transportConfig = new TransportConfig();
            endpoint.transportCreate(pjsip_transport_type_e.PJSIP_TRANSPORT_UDP, transportConfig);
            endpoint.libStart();
        }

        public static SoftPhone Instance
        {
            get
            {
                if (instance == null || instance._isDisposed)
                {
                    lock (syncRoot)
                    {
                        if (instance == null || instance._isDisposed)
                            instance = new SoftPhone();
                    }
                }

                return instance;
            }
        }

        public void connectServer(string idUri, string registrarUri, string realm, string username, string password)
        {

            if (idUri == _idUri
                && registrarUri == _registrarUri
                && realm == _realm
                && username == _username
                && password == _password)
            {
                return;
            }

            disconnectServer();

            if (idUri.Length == 0
                || registrarUri.Length == 0
                || realm.Length == 0
                || username.Length == 0
                || password.Length == 0)
            {
                return;
            }

            _idUri = idUri;
            _registrarUri = registrarUri;
            _realm = realm;
            _username = username;
            _password = password;

            AccountConfig accountConfig = new AccountConfig();

            accountConfig.regConfig.timeoutSec = 1800;
            accountConfig.regConfig.firstRetryIntervalSec = 10;
            accountConfig.regConfig.retryIntervalSec = 10;

            accountConfig.idUri = _idUri;
            accountConfig.regConfig.registrarUri = _registrarUri;
            AuthCredInfo cred = new AuthCredInfo("digest", _realm, _username, 0, _password);
            accountConfig.sipConfig.authCreds.Add(cred);

            account = new CustomAccount(this);
            account.create(accountConfig);

        }

        public void disconnectServer()
        {
            if (account != null)
            {
                endpoint.hangupAllCalls();
                account.Dispose();
                account = null;
            }

            _idUri = "";
            _registrarUri = "";
            _realm = "";
            _username = "";
            _password = "";

            ConnectionState oldConnectionState = connectionState;
            connectionState = ConnectionState.Disconnected;

            if (connectionState != oldConnectionState)
            {
                EventHandler<ConnectionState> handler = ConnectionStateChanged;
                if (handler != null)
                {
                    handler(this, connectionState);
                }
            }
        }


        public void OnRegStateUpdated(OnRegStateParam param)
        {
            ConnectionState oldConnectionState = connectionState;

            if (param.code == pjsip_status_code.PJSIP_SC_OK)
            {
                if (connectionState != ConnectionState.ConnectedTransmiting)
                {
                    connectionState = ConnectionState.Connected;
                }
            }
            else if (param.code == pjsip_status_code.PJSIP_SC_BAD_GATEWAY)
            {
                connectionState = ConnectionState.NoInternet;
            }
            else
            {
                connectionState = ConnectionState.UnableToConnect;
            }

            if (connectionState != oldConnectionState)
            {
                EventHandler<ConnectionState> handler = ConnectionStateChanged;
                if (handler != null)
                {
                    handler(this, connectionState);
                }
            }
        }

        public void OnIncomingCall(OnIncomingCallParam param)
        {
            CustomCall call = new CustomCall(this, account, param.callId);
            CallOpParam opParam = new CallOpParam();
            opParam.statusCode = pjsip_status_code.PJSIP_SC_OK;
            call.answer(opParam);
            callList.Add(call);
            notifyCallListChanged();
        }

        public void OnCallStateUpdated(CustomCall call)
        {
            CallInfo callInfo = call.getInfo();
            if (callInfo.state == pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED)
            {

                bool participantFound = false;
                foreach (Participant participant in userList)
                {
                    if (participant.PhoneNumber != null && participant.PhoneNumber.Length > 2 && callInfo.remoteUri != null && callInfo.remoteUri.Contains(participant.PhoneNumber.Substring(1)))
                    {
                        participantFound = true;
                        call.ParticipantName = participant.Name;
                        call.State = CallState.ListeningWelcomeSound;
                        call.startPlayer("WELCOME.WAV");
                        break;
                    }
                }

                if (participantFound == false)
                {
                    call.startPlayer("INSTRUCTIONS.WAV");
                    call.State = CallState.ListeningInstructionSound;
                }

                notifyCallListChanged();
            }
            else if (callInfo.state == pjsip_inv_state.PJSIP_INV_STATE_DISCONNECTED)
            {
                callList.Remove(call);
                call.Dispose();
                notifyCallListChanged();
            }
        }

        public void OnCallDtmfDigit(CustomCall call, OnDtmfDigitParam param)
        {
            string dtmf = param.digit;

            if (call.State == CallState.ListeningInstructionSound || call.State == CallState.ListeningCodeErrorSound)
            {
                call.State = CallState.TypingAccessCode;
                call.stopPlayer();
            }

            if (call.State == CallState.TypingAccessCode)
            {
                int intDtmf;
                if (dtmf.Equals(DTMF_SHARP))
                {
                    bool participantFound = false;
                    foreach (Participant participant in userList)
                    {
                        if (participant.Code.Equals(call.AccessCode))
                        {
                            participantFound = true;
                            call.ParticipantName = participant.Name;
                            call.State = CallState.ListeningWelcomeSound;
                            call.startPlayer("WELCOME.WAV");
                            break;
                        }
                    }

                    if (!participantFound)
                    {
                        call.State = CallState.ListeningCodeErrorSound;
                        call.startPlayer("CODE_ERROR.WAV");
                    }
                    notifyCallListChanged();
                }
                else if (int.TryParse(dtmf, out intDtmf))
                {
                    call.AccessCode += dtmf;
                    if (call.AccessCode.Length > 4)
                    {
                        call.AccessCode = call.AccessCode.Substring(call.AccessCode.Length - 4);
                    }
                }
            }
            else if (call.State == CallState.ListeningMeeting && dtmf.Equals(DTMF_REQUEST_TO_SPEAK))
            {
                call.State = CallState.RequestToSpeak;
                // Move call on top of the list
                callList.Remove(call);
                callList.Insert(0, call);
                notifyCallListChanged();
            }
            else if (call.State == CallState.RequestToSpeak && dtmf.Equals(DTMF_CANCEL_REQUEST_TO_SPEAK))
            {
                call.State = CallState.ListeningMeeting;
                notifyCallListChanged();
            }
            else if (call.State == CallState.Speaking && dtmf.Equals(DTMF_CANCEL_REQUEST_TO_SPEAK))
            {
                disconnectMicrophone(call);
            }
            else if (call.State == CallState.ListeningWelcomeSound && dtmf.Equals(DTMF_STAR))
            {
                call.stopPlayer();
                if (connectionState == ConnectionState.ConnectedTransmiting)
                {
                    try
                    {
                        endpoint.audDevManager().getCaptureDevMedia().startTransmit(call.audioMedia);
                    }
                    catch (Exception) {}

                    call.State = CallState.ListeningMeeting;
                }
                else
                {
                    call.startPlayer("HOLD.WAV");
                    call.State = CallState.ListeningHoldSound;
                }
                notifyCallListChanged();
            }
        }

        public void StartConference()
        {
            if (connectionState == ConnectionState.Connected)
            {
                foreach (CustomCall call in callList)
                {
                    if (call.State == CallState.ListeningHoldSound)
                    {
                        call.stopPlayer();

                        try
                        {
                            endpoint.audDevManager().getCaptureDevMedia().startTransmit(call.audioMedia);
                        } catch(Exception) {}

                        call.State = CallState.ListeningMeeting;
                    }
                }
                connectionState = ConnectionState.ConnectedTransmiting;
                notifyConnectionStateChanged();
                notifyCallListChanged();
            }
        }

        public void StopConference()
        {
            if (connectionState == ConnectionState.ConnectedTransmiting)
            {
                foreach (CustomCall call in callList)
                {
                    if (call.State == CallState.ListeningMeeting
                        || call.State == CallState.RequestToSpeak
                        || call.State == CallState.Speaking)
                    {
                        endpoint.audDevManager().getCaptureDevMedia().stopTransmit(call.audioMedia);
                        call.startPlayer("GOODBYE.WAV");
                        call.State = CallState.ListeningGoodbyeSound;
                    }
                }
                connectionState = ConnectionState.Connected;
                notifyConnectionStateChanged();
                notifyCallListChanged();
            }
        }
        
        public void connectMicrophone(int callId)
        {
            foreach (CustomCall call in callList)
            {
                if (call.Id == callId)
                {
                    connectMicrophone(call);
                    break;
                }
            }
        }

        public void connectMicrophone(CustomCall call)
        {
            if (call.State == CallState.RequestToSpeak)
            {
                call.dialDtmf("9");
                call.State = CallState.Speaking;
                call.audioMedia.startTransmit(endpoint.audDevManager().getPlaybackDevMedia());
                notifyCallListChanged();
            }
        }


        public void disconnectMicrophone(int callId)
        {
            foreach (CustomCall call in callList)
            {
                if (call.Id == callId)
                {
                    disconnectMicrophone(call);
                    break;
                }
            }
        }

        public void disconnectMicrophone(CustomCall call)
        {
            if (call.State == CallState.Speaking)
            {
                call.dialDtmf("11");
                call.State = CallState.ListeningMeeting;
                call.audioMedia.stopTransmit(endpoint.audDevManager().getPlaybackDevMedia());
                notifyCallListChanged();
            }
        }


        public void cancelSpeakRequest(int callId)
        {
            foreach (CustomCall call in callList)
            {
                if (call.Id == callId)
                {
                    cancelSpeakRequest(call);
                    break;
                }
            }
        }

        public void cancelSpeakRequest(CustomCall call)
        {
            if (call.State == CallState.RequestToSpeak)
            {
                call.dialDtmf("11");
                call.State = CallState.ListeningMeeting;
                notifyCallListChanged();
            }
        }



        private void notifyCallListChanged()
        {
            EventHandler<List<CustomCall>> handler = CallListChanged;
            if (handler != null)
            {
                handler(this, callList);
            }
        }

        private void notifyConnectionStateChanged()
        {
            EventHandler<ConnectionState> handler = ConnectionStateChanged;
            if (handler != null)
            {
                handler(this, connectionState);
            }
        }

        public event EventHandler<ConnectionState> ConnectionStateChanged;
        public event EventHandler<List<CustomCall>> CallListChanged;


        public void Dispose()
        {
            if (!_isDisposed)
            {
                foreach (CustomCall call in callList)
                {
                    call.Dispose();
                }

                if (account != null)
                {
                    account.Dispose();
                }

                if (endpoint != null)
                {
                    endpoint.libDestroy();
                    endpoint.Dispose();
                }

                _isDisposed = true;
            }
        }
    }

    public enum CallState
    {
        Connecting = 0,
        ListeningInstructionSound = 1,
        TypingAccessCode = 2,
        ListeningCodeErrorSound = 3,
        ListeningWelcomeSound = 4,
        ListeningHoldSound = 5,
        ListeningMeeting = 6,
        RequestToSpeak = 7,
        Speaking = 8,
        ListeningGoodbyeSound = 9
    };

    public enum ConnectionState
    {
        NoInternet = 1,
        UnableToConnect = 2,
        Disconnected = 3,
        Connected = 4,
        ConnectedTransmiting = 5
    };

}
