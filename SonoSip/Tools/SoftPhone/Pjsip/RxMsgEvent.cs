/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.12
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace pjsua2 {

using System;
using System.Runtime.InteropServices;

public class RxMsgEvent : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal RxMsgEvent(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(RxMsgEvent obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~RxMsgEvent() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          pjsua2PINVOKE.delete_RxMsgEvent(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public SipRxData rdata {
    set {
      pjsua2PINVOKE.RxMsgEvent_rdata_set(swigCPtr, SipRxData.getCPtr(value));
    } 
    get {
      IntPtr cPtr = pjsua2PINVOKE.RxMsgEvent_rdata_get(swigCPtr);
      SipRxData ret = (cPtr == IntPtr.Zero) ? null : new SipRxData(cPtr, false);
      return ret;
    } 
  }

  public RxMsgEvent() : this(pjsua2PINVOKE.new_RxMsgEvent(), true) {
  }

}

}
