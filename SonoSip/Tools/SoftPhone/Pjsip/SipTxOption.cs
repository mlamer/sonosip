/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.12
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace pjsua2 {

using System;
using System.Runtime.InteropServices;

public class SipTxOption : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal SipTxOption(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(SipTxOption obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~SipTxOption() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          pjsua2PINVOKE.delete_SipTxOption(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public string targetUri {
    set {
      pjsua2PINVOKE.SipTxOption_targetUri_set(swigCPtr, value);
      if (pjsua2PINVOKE.SWIGPendingException.Pending) throw pjsua2PINVOKE.SWIGPendingException.Retrieve();
    } 
    get {
      string ret = pjsua2PINVOKE.SipTxOption_targetUri_get(swigCPtr);
      if (pjsua2PINVOKE.SWIGPendingException.Pending) throw pjsua2PINVOKE.SWIGPendingException.Retrieve();
      return ret;
    } 
  }

  public SipHeaderVector headers {
    set {
      pjsua2PINVOKE.SipTxOption_headers_set(swigCPtr, SipHeaderVector.getCPtr(value));
    } 
    get {
      IntPtr cPtr = pjsua2PINVOKE.SipTxOption_headers_get(swigCPtr);
      SipHeaderVector ret = (cPtr == IntPtr.Zero) ? null : new SipHeaderVector(cPtr, false);
      return ret;
    } 
  }

  public string contentType {
    set {
      pjsua2PINVOKE.SipTxOption_contentType_set(swigCPtr, value);
      if (pjsua2PINVOKE.SWIGPendingException.Pending) throw pjsua2PINVOKE.SWIGPendingException.Retrieve();
    } 
    get {
      string ret = pjsua2PINVOKE.SipTxOption_contentType_get(swigCPtr);
      if (pjsua2PINVOKE.SWIGPendingException.Pending) throw pjsua2PINVOKE.SWIGPendingException.Retrieve();
      return ret;
    } 
  }

  public string msgBody {
    set {
      pjsua2PINVOKE.SipTxOption_msgBody_set(swigCPtr, value);
      if (pjsua2PINVOKE.SWIGPendingException.Pending) throw pjsua2PINVOKE.SWIGPendingException.Retrieve();
    } 
    get {
      string ret = pjsua2PINVOKE.SipTxOption_msgBody_get(swigCPtr);
      if (pjsua2PINVOKE.SWIGPendingException.Pending) throw pjsua2PINVOKE.SWIGPendingException.Retrieve();
      return ret;
    } 
  }

  public SipMediaType multipartContentType {
    set {
      pjsua2PINVOKE.SipTxOption_multipartContentType_set(swigCPtr, SipMediaType.getCPtr(value));
    } 
    get {
      IntPtr cPtr = pjsua2PINVOKE.SipTxOption_multipartContentType_get(swigCPtr);
      SipMediaType ret = (cPtr == IntPtr.Zero) ? null : new SipMediaType(cPtr, false);
      return ret;
    } 
  }

  public SipMultipartPartVector multipartParts {
    set {
      pjsua2PINVOKE.SipTxOption_multipartParts_set(swigCPtr, SipMultipartPartVector.getCPtr(value));
    } 
    get {
      IntPtr cPtr = pjsua2PINVOKE.SipTxOption_multipartParts_get(swigCPtr);
      SipMultipartPartVector ret = (cPtr == IntPtr.Zero) ? null : new SipMultipartPartVector(cPtr, false);
      return ret;
    } 
  }

  public bool isEmpty() {
    bool ret = pjsua2PINVOKE.SipTxOption_isEmpty(swigCPtr);
    return ret;
  }

  public SipTxOption() : this(pjsua2PINVOKE.new_SipTxOption(), true) {
  }

}

}
