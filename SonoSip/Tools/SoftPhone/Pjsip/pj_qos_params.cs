/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.12
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace pjsua2 {

using System;
using System.Runtime.InteropServices;

public class pj_qos_params : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal pj_qos_params(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(pj_qos_params obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~pj_qos_params() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          pjsua2PINVOKE.delete_pj_qos_params(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public byte flags {
    set {
      pjsua2PINVOKE.pj_qos_params_flags_set(swigCPtr, value);
    } 
    get {
      byte ret = pjsua2PINVOKE.pj_qos_params_flags_get(swigCPtr);
      return ret;
    } 
  }

  public byte dscp_val {
    set {
      pjsua2PINVOKE.pj_qos_params_dscp_val_set(swigCPtr, value);
    } 
    get {
      byte ret = pjsua2PINVOKE.pj_qos_params_dscp_val_get(swigCPtr);
      return ret;
    } 
  }

  public byte so_prio {
    set {
      pjsua2PINVOKE.pj_qos_params_so_prio_set(swigCPtr, value);
    } 
    get {
      byte ret = pjsua2PINVOKE.pj_qos_params_so_prio_get(swigCPtr);
      return ret;
    } 
  }

  public pj_qos_wmm_prio wmm_prio {
    set {
      pjsua2PINVOKE.pj_qos_params_wmm_prio_set(swigCPtr, (int)value);
    } 
    get {
      pj_qos_wmm_prio ret = (pj_qos_wmm_prio)pjsua2PINVOKE.pj_qos_params_wmm_prio_get(swigCPtr);
      return ret;
    } 
  }

  public pj_qos_params() : this(pjsua2PINVOKE.new_pj_qos_params(), true) {
  }

}

}
