/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.12
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace pjsua2 {

using System;
using System.Runtime.InteropServices;

public class LogWriter : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal LogWriter(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(LogWriter obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~LogWriter() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          pjsua2PINVOKE.delete_LogWriter(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public virtual void write(LogEntry entry) {
    pjsua2PINVOKE.LogWriter_write(swigCPtr, LogEntry.getCPtr(entry));
    if (pjsua2PINVOKE.SWIGPendingException.Pending) throw pjsua2PINVOKE.SWIGPendingException.Retrieve();
  }

  public LogWriter() : this(pjsua2PINVOKE.new_LogWriter(), true) {
    SwigDirectorConnect();
  }

  private void SwigDirectorConnect() {
    if (SwigDerivedClassHasMethod("write", swigMethodTypes0))
      swigDelegate0 = new SwigDelegateLogWriter_0(SwigDirectorwrite);
    pjsua2PINVOKE.LogWriter_director_connect(swigCPtr, swigDelegate0);
  }

  private bool SwigDerivedClassHasMethod(string methodName, Type[] methodTypes) {
    System.Reflection.MethodInfo methodInfo = this.GetType().GetMethod(methodName, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance, null, methodTypes, null);
    bool hasDerivedMethod = methodInfo.DeclaringType.IsSubclassOf(typeof(LogWriter));
    return hasDerivedMethod;
  }

  private void SwigDirectorwrite(IntPtr entry) {
    write(new LogEntry(entry, false));
  }

  public delegate void SwigDelegateLogWriter_0(IntPtr entry);

  private SwigDelegateLogWriter_0 swigDelegate0;

  private static Type[] swigMethodTypes0 = new Type[] { typeof(LogEntry) };
}

}
