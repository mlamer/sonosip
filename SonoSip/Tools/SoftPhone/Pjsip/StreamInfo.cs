/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.12
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace pjsua2 {

using System;
using System.Runtime.InteropServices;

public class StreamInfo : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal StreamInfo(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(StreamInfo obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~StreamInfo() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          pjsua2PINVOKE.delete_StreamInfo(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public pjmedia_type type {
    set {
      pjsua2PINVOKE.StreamInfo_type_set(swigCPtr, (int)value);
    } 
    get {
      pjmedia_type ret = (pjmedia_type)pjsua2PINVOKE.StreamInfo_type_get(swigCPtr);
      return ret;
    } 
  }

  public pjmedia_tp_proto proto {
    set {
      pjsua2PINVOKE.StreamInfo_proto_set(swigCPtr, (int)value);
    } 
    get {
      pjmedia_tp_proto ret = (pjmedia_tp_proto)pjsua2PINVOKE.StreamInfo_proto_get(swigCPtr);
      return ret;
    } 
  }

  public pjmedia_dir dir {
    set {
      pjsua2PINVOKE.StreamInfo_dir_set(swigCPtr, (int)value);
    } 
    get {
      pjmedia_dir ret = (pjmedia_dir)pjsua2PINVOKE.StreamInfo_dir_get(swigCPtr);
      return ret;
    } 
  }

  public string remoteRtpAddress {
    set {
      pjsua2PINVOKE.StreamInfo_remoteRtpAddress_set(swigCPtr, value);
      if (pjsua2PINVOKE.SWIGPendingException.Pending) throw pjsua2PINVOKE.SWIGPendingException.Retrieve();
    } 
    get {
      string ret = pjsua2PINVOKE.StreamInfo_remoteRtpAddress_get(swigCPtr);
      if (pjsua2PINVOKE.SWIGPendingException.Pending) throw pjsua2PINVOKE.SWIGPendingException.Retrieve();
      return ret;
    } 
  }

  public string remoteRtcpAddress {
    set {
      pjsua2PINVOKE.StreamInfo_remoteRtcpAddress_set(swigCPtr, value);
      if (pjsua2PINVOKE.SWIGPendingException.Pending) throw pjsua2PINVOKE.SWIGPendingException.Retrieve();
    } 
    get {
      string ret = pjsua2PINVOKE.StreamInfo_remoteRtcpAddress_get(swigCPtr);
      if (pjsua2PINVOKE.SWIGPendingException.Pending) throw pjsua2PINVOKE.SWIGPendingException.Retrieve();
      return ret;
    } 
  }

  public uint txPt {
    set {
      pjsua2PINVOKE.StreamInfo_txPt_set(swigCPtr, value);
    } 
    get {
      uint ret = pjsua2PINVOKE.StreamInfo_txPt_get(swigCPtr);
      return ret;
    } 
  }

  public uint rxPt {
    set {
      pjsua2PINVOKE.StreamInfo_rxPt_set(swigCPtr, value);
    } 
    get {
      uint ret = pjsua2PINVOKE.StreamInfo_rxPt_get(swigCPtr);
      return ret;
    } 
  }

  public string codecName {
    set {
      pjsua2PINVOKE.StreamInfo_codecName_set(swigCPtr, value);
      if (pjsua2PINVOKE.SWIGPendingException.Pending) throw pjsua2PINVOKE.SWIGPendingException.Retrieve();
    } 
    get {
      string ret = pjsua2PINVOKE.StreamInfo_codecName_get(swigCPtr);
      if (pjsua2PINVOKE.SWIGPendingException.Pending) throw pjsua2PINVOKE.SWIGPendingException.Retrieve();
      return ret;
    } 
  }

  public uint codecClockRate {
    set {
      pjsua2PINVOKE.StreamInfo_codecClockRate_set(swigCPtr, value);
    } 
    get {
      uint ret = pjsua2PINVOKE.StreamInfo_codecClockRate_get(swigCPtr);
      return ret;
    } 
  }

  public SWIGTYPE_p_void codecParam {
    set {
      pjsua2PINVOKE.StreamInfo_codecParam_set(swigCPtr, SWIGTYPE_p_void.getCPtr(value));
    } 
    get {
      IntPtr cPtr = pjsua2PINVOKE.StreamInfo_codecParam_get(swigCPtr);
      SWIGTYPE_p_void ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_void(cPtr, false);
      return ret;
    } 
  }

  public StreamInfo() : this(pjsua2PINVOKE.new_StreamInfo(), true) {
  }

}

}
