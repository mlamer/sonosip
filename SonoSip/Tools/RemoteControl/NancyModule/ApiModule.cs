﻿namespace SonoSip.Tools.RemoteControl.NancyModule
{
    using Nancy;
    using Nancy.Security;
    using SonoSip.Tools.SoftPhone;
    using System;
    using System.Windows;
    using System.Windows.Threading;

    public class ApiModule : NancyModule
    {
        public ApiModule()
            : base("/api")
        {
            this.RequiresAuthentication();

            Get["/connectMicrophone/{callId}"] = parameters =>
            {
                int callId = (int)parameters.callId;
                // Thread safe call
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => SoftPhone.Instance.connectMicrophone(callId)));
                return new Response();
            };
            Get["/disconnectMicrophone/{callId}"] = parameters =>
            {
                int callId = (int)parameters.callId;
                // Thread safe call
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => SoftPhone.Instance.disconnectMicrophone(callId)));
                return new Response();
            };
            Get["/cancelSpeakRequest/{callId}"] = parameters =>
            {
                int callId = (int)parameters.callId;
                // Thread safe call
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => SoftPhone.Instance.cancelSpeakRequest(callId)));
                return new Response();
            };
        }
    }
}
