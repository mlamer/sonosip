﻿namespace SonoSip.Tools.RemoteControl
{
    using Nancy;
    using Owin;

    internal class CustomStartup
    {
        public void Configuration(IAppBuilder app)
        {
            StaticConfiguration.DisableErrorTraces = false;
            Nancy.Owin.NancyOptions nancyOptions = new Nancy.Owin.NancyOptions();
            nancyOptions.Bootstrapper = new CustomBootstrapper();

            app.UseNancy(nancyOptions);
        }
    }
}
