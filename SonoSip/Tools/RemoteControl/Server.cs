﻿namespace SonoSip.Tools.RemoteControl
{
    using Microsoft.Owin.Hosting;
    using SonoSip.Tools.SoftPhone;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Sockets;
    using System.Security.Principal;
    using Nancy.Hosting.Self;

    class Server
    {

        private static volatile Server instance;
        private static object syncRoot = new Object();
        private bool _isDisposed = false;

        private IDisposable _webApp;
        private int _port = 8989;
        
        private Server()
        {
        }

        public static Server Instance
        {
            get
            {
                if (instance == null || instance._isDisposed)
                {
                    lock (syncRoot)
                    {
                        if (instance == null || instance._isDisposed)
                            instance = new Server();
                    }
                }

                return instance;
            }
        }

        public void Start()
        {
            // Start listening on every adresse at specified port
            string url = string.Format("http://+:{0}/", _port);
            try
            {
                _webApp = WebApp.Start<CustomStartup>(url);
            }
            catch (System.Reflection.TargetInvocationException e)
            {
                // If we catch this exception is because we need to make a UrlReservation

                // Find localized "Everyone" user for netsh command 
                var sid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                var account = (NTAccount)sid.Translate(typeof(NTAccount));

                string command = string.Format("/c netsh advfirewall firewall add rule name=\"Sonosip remote control\" dir=in action=allow protocol=TCP localport={0} & netsh http add urlacl url={1} user=\"{2}\"", _port, url, account.Value);
                // Run netsh command with Nancy.Hosting.Self package helper
                if (UacHelper.RunElevated("cmd", command))
                {
                    _webApp = WebApp.Start<CustomStartup>(url);
                }
                else
                {
                    throw new Exception("Unable to run remote control server due to URL reservation failure or firewall issue", e);
                }
            }
        }

        public void Stop()
        {
            if (_webApp != null)
            {
                _webApp.Dispose();
                _webApp = null;
            }
        }

        public string GetRemoteUrl()
        {
            string hostName = Dns.GetHostName();

            // Host address URI
            var hostEntry = Dns.GetHostEntry(hostName);
            foreach (var ipAddress in hostEntry.AddressList)
            {
                if (ipAddress.AddressFamily == AddressFamily.InterNetwork)  // IPv4 addresses only
                {
                    var addrBytes = ipAddress.GetAddressBytes();
                    return string.Format("http://{0}.{1}.{2}.{3}:{4}", addrBytes[0], addrBytes[1], addrBytes[2], addrBytes[3], _port);
                }
            }

            // Localhost URI
            return string.Format("http://localhost:{0}", _port);

        }

        public void Dispose()
        {
            if (!_isDisposed)
            {
                Stop();
                _isDisposed = true;
            }
        }
    }

}
