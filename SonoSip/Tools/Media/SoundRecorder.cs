﻿using NAudio.CoreAudioApi;
using NAudio.Lame;
using NAudio.Wave;
using System;

namespace SonoSip.Tools.Media
{
    class SoundRecorder
    {
        private static volatile SoundRecorder instance;
        private static object syncRoot = new Object();

        private String recordFileName;
        private Boolean isRecording;

        private LameMP3FileWriter lameMP3FileWriter;
        private IWaveIn waveIn;

        private SoundRecorder()
        {
            isRecording = false;
        }

        public static SoundRecorder Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new SoundRecorder();
                    }
                }

                return instance;
            }
        }

        public void StartRecord(String fileName)
        {
            StopRecord();
            recordFileName = fileName;
            waveIn = new WasapiCapture();
            waveIn.DataAvailable += waveIn_DataAvailable;
            waveIn.RecordingStopped += waveIn_RecordingStopped;

            lameMP3FileWriter = new LameMP3FileWriter(fileName, waveIn.WaveFormat, 96);

            waveIn.StartRecording();
            isRecording = true;
            OnRecorderStateChanged();
        }

        public void StopRecord()
        {
            if (isRecording)
            {
                waveIn.StopRecording();
                lameMP3FileWriter.Flush();
                lameMP3FileWriter.Dispose();
                waveIn.Dispose();
            }
        }

        private void waveIn_RecordingStopped(object sender, StoppedEventArgs e)
        {
            isRecording = false;
            OnRecorderStateChanged();
        }

        private void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            if (lameMP3FileWriter != null)
            {
                lameMP3FileWriter.Write(e.Buffer, 0, e.BytesRecorded);
            }
        }


        protected virtual void OnRecorderStateChanged()
        {
            RecorderStateChangedEventArgs args = new RecorderStateChangedEventArgs();
            args.recording = isRecording;
            args.fileName = recordFileName;

            EventHandler<RecorderStateChangedEventArgs> handler = RecorderStateChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        public event EventHandler<RecorderStateChangedEventArgs> RecorderStateChanged;


    }

    public class RecorderStateChangedEventArgs : EventArgs
    {
        public Boolean recording { get; set; }
        public String fileName { get; set; }
    }
}
