﻿using NAudio.Wave;
using System;

namespace SonoSip.Tools.Media
{
    class SoundPlayer
    {
        private static volatile SoundPlayer instance;
        private static object syncRoot = new Object();

        Boolean playRandom;
        Boolean isPlaying;
        String songDirectory;

        public int PlayVolume;
        public int RandomPlayVolume;


        IWavePlayer waveOutDevice;
        AudioFileReader audioFileReader;

        private SoundPlayer()
        {
            playRandom = false;
            PlayVolume = 100;
            RandomPlayVolume = 100;
        }

        public static SoundPlayer Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new SoundPlayer();
                    }
                }

                return instance;
            }
        }

        public void Play(String fileName)
        {
            Stop();
            playRandom = false;

            waveOutDevice = new WaveOut();
            audioFileReader = new AudioFileReader(fileName);
            audioFileReader.Volume = (float)PlayVolume / 100;
            waveOutDevice.Init(audioFileReader);
            waveOutDevice.PlaybackStopped += PlaybackStopped;
            waveOutDevice.Play();

            isPlaying = true;
            OnPlayerStateChanged();
        }

        private void PlaybackStopped(object sender, StoppedEventArgs e)
        {
            if (playRandom)
            {
                PlayRandom(songDirectory);
            }
            else
            {
                clean();
                isPlaying = false;
                OnPlayerStateChanged();
            }
        }

        public void PlayRandom(String directory)
        {
            Stop();

            if (System.IO.Directory.Exists(directory))
            {
                playRandom = true;
                songDirectory = directory;

                string[] musicFiles = System.IO.Directory.GetFiles(songDirectory, "*.mp3");

                if (musicFiles.Length > 0)
                {
                    var random = new Random();
                    waveOutDevice = new WaveOut();
                    audioFileReader = new AudioFileReader(musicFiles[random.Next(0, musicFiles.Length)]);
                    audioFileReader.Volume = (float)RandomPlayVolume / 100;
                    waveOutDevice.Init(audioFileReader);
                    waveOutDevice.PlaybackStopped += PlaybackStopped;
                    waveOutDevice.Play();

                    isPlaying = true;
                    OnPlayerStateChanged();
                }
            }
        }

        public void Stop()
        {
            if (isPlaying)
            {
                if (waveOutDevice != null)
                {
                    waveOutDevice.Stop();
                }

                clean();
                isPlaying = false;
                OnPlayerStateChanged();
            }
        }

        private void clean()
        {
            if (audioFileReader != null)
            {
                audioFileReader.Dispose();
                audioFileReader = null;
            }
            if (waveOutDevice != null)
            {
                waveOutDevice.Dispose();
                waveOutDevice = null;
            }
        }



        protected virtual void OnPlayerStateChanged()
        {
            PlayerStateChangedEventArgs args = new PlayerStateChangedEventArgs();
            args.playing = isPlaying;
            args.random = playRandom;

            EventHandler<PlayerStateChangedEventArgs> handler = PlayerStateChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        public event EventHandler<PlayerStateChangedEventArgs> PlayerStateChanged;


    }

    public class PlayerStateChangedEventArgs : EventArgs
    {
        public Boolean playing { get; set; }
        public Boolean random { get; set; }
    }
}
