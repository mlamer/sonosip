﻿using MahApps.Metro.Controls.Dialogs;
using Ookii.Dialogs.Wpf;
using SonoSip.Models;
using SonoSip.Tools.Media;
using SonoSip.Tools.RemoteControl;
using SonoSip.Tools.SoftPhone;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace SonoSip
{
    public partial class MainWindow
    {
        private List<CustomCall> CallList;
        private List<Cantique> CantiqueList;

        public MainWindow()
        {
            InitializeComponent();

            ConnectionStateTextBlock.Content = new TextBlock { Text = "Module de retransmission inactif", VerticalAlignment = VerticalAlignment.Bottom, Margin = new Thickness(5, 0, 0, 6) };

            if (Properties.Settings.Default.Congregations == null)
            {
                Properties.Settings.Default.Congregations = new System.Collections.ArrayList();
            }
            if (Properties.Settings.Default.Participants == null)
            {
                Properties.Settings.Default.Participants = new System.Collections.ArrayList();
            }


            CallList = new List<CustomCall>();

            this.Closed += disposeTools;

            SoundRecorder.Instance.RecorderStateChanged += RecorderStateChanged;
            SoundPlayer.Instance.PlayerStateChanged += PlayerStateChanged;
            SoftPhone.Instance.ConnectionStateChanged += ConnectionStateChanged;
            SoftPhone.Instance.CallListChanged += CallListChanged;
            SoftPhone.Instance.resourcesPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "Resources", "fr");

            Server.Instance.Start();

            RemoteUriHyperlink.Inlines.Add(Server.Instance.GetRemoteUrl());
            RemoteUriHyperlink.NavigateUri = new Uri(Server.Instance.GetRemoteUrl());

            updateCongregationList();
            updateParticipantsList();
            applySettings();
        }

        private void disposeTools(object sender, EventArgs e)
        {
            SoftPhone.Instance.Dispose();
            Server.Instance.Dispose();
        }


        private void ToggleSettingsFlyout(object sender, RoutedEventArgs e)
        {
            settingsFlyout.IsOpen = !settingsFlyout.IsOpen;
        }

        private void SaveSettingsFlyout(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.Save();
            applySettings();
            settingsFlyout.IsOpen = !settingsFlyout.IsOpen;
        }

        private void SelectCantiqueFolder(object sender, RoutedEventArgs e)
        {
            VistaFolderBrowserDialog selectFolderDialog = new VistaFolderBrowserDialog();
            Nullable<bool> result = selectFolderDialog.ShowDialog();

            if (result == true)
            {
                Properties.Settings.Default.CantiquesFolder = selectFolderDialog.SelectedPath;
            }
        }

        private void SelectRecordsFolder(object sender, RoutedEventArgs e)
        {
            VistaFolderBrowserDialog selectFolderDialog = new VistaFolderBrowserDialog();
            Nullable<bool> result = selectFolderDialog.ShowDialog();

            if (result == true)
            {
                Properties.Settings.Default.RecordsFolder = selectFolderDialog.SelectedPath;
            }
        }

        private void HyperlinkRequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        private async void AddCongregation(object sender, RoutedEventArgs e)
        {
            var congregationName = await this.ShowInputAsync("Ajouter une congrégation", "Quel est le nom de la congrégation ?");

            if (congregationName == null)
            {
                return;
            }
            Properties.Settings.Default.Congregations.Add(congregationName);
            updateCongregationList();
        }

        private async void AddParticipants(object sender, RoutedEventArgs e)
        {
            MetroDialogSettings metroDialogSettings = new MetroDialogSettings();
            metroDialogSettings.AffirmativeButtonText = "Valider";
            metroDialogSettings.NegativeButtonText = "Annuler";

            string participantName = await this.ShowInputAsync("Ajouter un participant", "Quel est le nom du nouveau participant ?", metroDialogSettings);

            if (participantName == null)
            {
                return;
            }

            string phoneNumber = await this.ShowInputAsync("Ajouter un participant", "Quel est le numéro de téléphone à utiliser pour la connexion sans mot de passe ?", metroDialogSettings);
            

            List<string> existingCodes = new List<string>();

            foreach (String jsonParticipant in Properties.Settings.Default.Participants)
            {
                Participant participant = Newtonsoft.Json.JsonConvert.DeserializeObject<Participant>(jsonParticipant);
                existingCodes.Add(participant.Code);
            }
            metroDialogSettings.NegativeButtonText = "Auto";

            string participantCode = await this.ShowInputAsync("Ajouter un participant", "Quel est le code d'accès du nouveau participant ?" + Environment.NewLine + "Saisissez un code à 4 chiffres." + Environment.NewLine + "Laissez vide pour générer un code automatiquement.", metroDialogSettings);

            int intParticipantCode;
            do
            {
                if (participantCode == null)
                {
                    var random = new Random();
                    participantCode = random.Next(1000, 9999).ToString();
                    break;
                }
                else if (participantCode.Length == 4 
                    && int.TryParse(participantCode, out intParticipantCode) 
                    && !existingCodes.Contains(participantCode) 
                    && !participantCode.Equals("0000"))
                {
                    break;
                }
                else
                {
                    participantCode = await this.ShowInputAsync("Ajouter un participant", "Code incorrect ou déjà existant !" + Environment.NewLine + "Saisissez un code à 4 chiffres." + Environment.NewLine + "Laissez vide pour générer un code automatiquement.", metroDialogSettings);
                }

            } while (true);


            Participant newParticipant = new Participant(participantCode, participantName, phoneNumber);
            string newJsonParticipant = Newtonsoft.Json.JsonConvert.SerializeObject(newParticipant);

            Properties.Settings.Default.Participants.Add(newJsonParticipant);
            updateParticipantsList();
        }

        private void updateCongregationList()
        {
            CongregationList.ItemsSource = null;
            CongregationList.ItemsSource = Properties.Settings.Default.Congregations;
        }

        private void updateParticipantsList()
        {
            System.Collections.ArrayList participants = new System.Collections.ArrayList();

            foreach (String jsonParticipant in Properties.Settings.Default.Participants)
            {
                Participant participant = Newtonsoft.Json.JsonConvert.DeserializeObject<Participant>(jsonParticipant);
                participants.Add(participant);
            }

            ParticipantsList.ItemsSource = null;
            ParticipantsList.ItemsSource = participants;
        }

        private void CongregationListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CongregationList.SelectedIndex != -1)
            {
                RemoveCongregationButton.IsEnabled = true;
            }
            else
            {
                RemoveCongregationButton.IsEnabled = false;
            }
        }

        private void ParticipantListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ParticipantsList.SelectedIndex != -1)
            {
                RemoveParticipantButton.IsEnabled = true;
            }
            else
            {
                RemoveParticipantButton.IsEnabled = false;
            }
        }

        private void RemoveSelectedCongregation(object sender, RoutedEventArgs e)
        {
            if (CongregationList.SelectedIndex != -1)
            {
                Properties.Settings.Default.Congregations.RemoveAt(CongregationList.SelectedIndex);
                updateCongregationList();
            }
        }

        private void RemoveSelectedParticipant(object sender, RoutedEventArgs e)
        {
            if (ParticipantsList.SelectedIndex != -1)
            {
                Properties.Settings.Default.Participants.RemoveAt(ParticipantsList.SelectedIndex);
                updateParticipantsList();
            }
        }

        private void applySettings()
        {
            CongregationCombo.ItemsSource = null;
            CongregationCombo.ItemsSource = Properties.Settings.Default.Congregations;

            CantiqueList = new List<Cantique>();

            if (System.IO.Directory.Exists(Properties.Settings.Default.CantiquesFolder))
            {
                string[] musicFiles = System.IO.Directory.GetFiles(Properties.Settings.Default.CantiquesFolder, "*.mp3");
                foreach (string musicFile in musicFiles)
                {
                    TagLib.File mp3File = TagLib.File.Create(musicFile);
                    Cantique cantique = new Cantique();
                    cantique.FileName = musicFile;
                    cantique.Title = mp3File.Tag.Title;
                    CantiqueList.Add(cantique);
                }
            }

            CantiqueCombo.ItemsSource = null;
            CantiqueCombo.ItemsSource = CantiqueList;

            if (CantiqueList.Count > 0)
            {
                PlayRandomCantiqueButton.IsEnabled = true;
                PlayRandomCantiqueButton.Opacity = 1;
            }
            else
            {
                PlayCantiqueButton.IsEnabled = false;
                PlayCantiqueButton.Opacity = 0.6;
                PlayRandomCantiqueButton.IsEnabled = false;
                PlayRandomCantiqueButton.Opacity = 0.6;
            }

            SoundPlayer.Instance.PlayVolume = Properties.Settings.Default.CantiqueVolume;
            SoundPlayer.Instance.RandomPlayVolume = Properties.Settings.Default.FondVolume;

            List<Participant> userList = new List<Participant>();

            foreach (String jsonParticipant in Properties.Settings.Default.Participants)
            {
                Participant participant = Newtonsoft.Json.JsonConvert.DeserializeObject<Participant>(jsonParticipant);
                userList.Add(participant);
            }

            SoftPhone.Instance.userList = userList;

            try
            {
                SoftPhone.Instance.connectServer(
                    Properties.Settings.Default.SipId,
                    Properties.Settings.Default.SipRegistrar,
                    Properties.Settings.Default.SipRealm,
                    Properties.Settings.Default.SipUsername,
                    Properties.Settings.Default.SipPassword);
            }
            catch (Exception)
            {
                this.ShowMessageAsync("Oups !", "Il y a un petit souci, veuillez vérifier la configuration SIP.");
            }

            UserDatabase.ClearUserList();
            if (Properties.Settings.Default.RemotePassword.Length > 0)
            {
                UserDatabase.AddUser("SoftPhone Remote User", Properties.Settings.Default.RemotePassword, new List<string>() { "SoftPhoneControl" });
            }

        }

        private void ActivateMicrophone(object sender, RoutedEventArgs e)
        {
            CustomCall call = (CustomCall)((Button)sender).Tag;
            SoftPhone.Instance.connectMicrophone(call);
        }

        private void CancelMicrophone(object sender, RoutedEventArgs e)
        {
            CustomCall call = (CustomCall)((Button)sender).Tag;
            if (call.State == CallState.RequestToSpeak)
            {
                SoftPhone.Instance.cancelSpeakRequest(call);
            }
            else if (call.State == CallState.Speaking)
            {
                SoftPhone.Instance.disconnectMicrophone(call);
            }
        }

        private void StopRecord(object sender, RoutedEventArgs e)
        {
            SoundRecorder.Instance.StopRecord();
        }

        private void StartRecord(object sender, RoutedEventArgs e)
        {
            String basePath = System.IO.Path.Combine(Properties.Settings.Default.RecordsFolder, CongregationCombo.Text);
            System.IO.Directory.CreateDirectory(basePath);
            String fileName = System.IO.Path.Combine(basePath, String.Format("Enregistrement du {0:dd-MM-yyyy 'à' HH'H'mm} ( {1} ).mp3", DateTime.Now, MeetingTypeCombo.Text));
            try 
            {
                SoundRecorder.Instance.StartRecord(fileName);
            }
            catch (Exception)
            {
                this.ShowMessageAsync("Oups !", "Il y a un petit souci, vous êtes sûr que le micro est bien configuré ?");
            }
        }

        private void RecorderStateChanged(object sender, RecorderStateChangedEventArgs e)
        {
            if (e.recording)
            {
                StopRecordButton.Visibility = Visibility.Visible;
                StartRecordButton.Visibility = Visibility.Collapsed;
            }
            else
            {
                StopRecordButton.Visibility = Visibility.Collapsed;
                StartRecordButton.Visibility = Visibility.Visible;
            }
        }

        private void EnableStartRecordButton(object sender, SelectionChangedEventArgs e)
        {
            if (CongregationCombo.SelectedIndex != -1
                && MeetingTypeCombo.SelectedIndex != -1
                && Properties.Settings.Default.RecordsFolder != null
                && !Properties.Settings.Default.RecordsFolder.Equals(""))
            {
                StartRecordButton.IsEnabled = true;
                StartRecordButton.Opacity = 1;
            }
            else
            {
                StartRecordButton.IsEnabled = false;
                StartRecordButton.Opacity = 0.6;
            }
        }

        private void EnablePlayCantiqueButton(object sender, SelectionChangedEventArgs e)
        {
            if (CantiqueCombo.SelectedIndex != -1)
            {
                PlayCantiqueButton.IsEnabled = true;
                PlayCantiqueButton.Opacity = 1;
            }
            else
            {
                PlayCantiqueButton.IsEnabled = false;
                PlayCantiqueButton.Opacity = 0.6;
            }
        }

        private void PlayCantique(object sender, RoutedEventArgs e)
        {
            if (CantiqueCombo.SelectedIndex != -1)
            {
                Cantique selectedCantique = (Cantique)CantiqueCombo.SelectedItem;
                SoundPlayer.Instance.Play(selectedCantique.FileName);
            }
        }

        private void PlayRandomCantique(object sender, RoutedEventArgs e)
        {
            SoundPlayer.Instance.PlayRandom(Properties.Settings.Default.CantiquesFolder);
        }

        private void StopCantique(object sender, RoutedEventArgs e)
        {
            SoundPlayer.Instance.Stop();
        }

        private void PlayerStateChanged(object sender, PlayerStateChangedEventArgs e)
        {
            if (e.playing)
            {
                StopCantiqueButton.Visibility = Visibility.Visible;
                PlayButtonsGrid.Visibility = Visibility.Collapsed;
            }
            else
            {
                StopCantiqueButton.Visibility = Visibility.Collapsed;
                PlayButtonsGrid.Visibility = Visibility.Visible;
            }

        }

        // Thread safe call to UI
        private void CallListChanged(object sender, List<CustomCall> callList)
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => updateCallListItemControl(callList)));
        }

        private void updateCallListItemControl(List<CustomCall> callList)
        {
            CallListItemsControl.ItemsSource = null;
            CallListItemsControl.ItemsSource = callList;
        }

        // Thread safe call to UI
        private void ConnectionStateChanged(object sender, ConnectionState connectionState)
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => updateConnectionState(connectionState)));
        }

        private void updateConnectionState(ConnectionState connectionState)
        {
            StartConferenceButton.Visibility = Visibility.Visible;
            StopConferenceButton.Visibility = Visibility.Collapsed;
            StartConferenceButton.IsEnabled = false;
            StartConferenceButton.Opacity = 0.6;

            ConnectionStatusHBar1.Opacity = 0.3;
            ConnectionStatusHBar2.Opacity = 0.3;
            ConnectionStatusHBar3.Opacity = 0.3;
            ConnectionStatusInternet.Opacity = 0.3;
            ConnectionStatusServer.Opacity = 0.3;
            ConnectionStatusTransmition.Opacity = 0.3;

            if (connectionState == ConnectionState.Connected)
            {
                StartConferenceButton.Visibility = Visibility.Visible;
                StopConferenceButton.Visibility = Visibility.Collapsed;
                StartConferenceButton.IsEnabled = true;
                StartConferenceButton.Opacity = 1;

                ConnectionStatusHBar1.Opacity = 1;
                ConnectionStatusHBar2.Opacity = 1;
                ConnectionStatusInternet.Opacity = 1;
                ConnectionStatusServer.Opacity = 1;

                ConnectionStateTextBlock.Content = new TextBlock { Text = "Serveur connecté", VerticalAlignment = VerticalAlignment.Bottom, Margin = new Thickness(5, 0, 0, 6), Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 51, 181, 229)) };
            }
            else if (connectionState == ConnectionState.ConnectedTransmiting)
            {
                StartConferenceButton.Visibility = Visibility.Collapsed;
                StopConferenceButton.Visibility = Visibility.Visible;

                ConnectionStatusHBar1.Opacity = 1;
                ConnectionStatusHBar2.Opacity = 1;
                ConnectionStatusHBar3.Opacity = 1;
                ConnectionStatusInternet.Opacity = 1;
                ConnectionStatusServer.Opacity = 1;
                ConnectionStatusTransmition.Opacity = 1;

                ConnectionStateTextBlock.Content = new TextBlock { Text = "Retransmission en cours", VerticalAlignment = VerticalAlignment.Bottom, Margin = new Thickness(5, 0, 0, 6), Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 135, 191, 77)) };
            }
            else if (connectionState == ConnectionState.Disconnected)
            {
                ConnectionStateTextBlock.Content = new TextBlock { Text = "Module de retransmission inactif", VerticalAlignment = VerticalAlignment.Bottom, Margin = new Thickness(5, 0, 0, 6), Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 112, 112, 112)) };
            }
            else if (connectionState == ConnectionState.NoInternet)
            {
                ConnectionStateTextBlock.Content = new TextBlock { Text = "Pas de connexion à internet", VerticalAlignment = VerticalAlignment.Bottom, Margin = new Thickness(5, 0, 0, 6), Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 221, 68, 68)) };
            }
            else if (connectionState == ConnectionState.UnableToConnect)
            {
                ConnectionStatusHBar1.Opacity = 1;
                ConnectionStatusInternet.Opacity = 1;
                ConnectionStateTextBlock.Content = new TextBlock { Text = "Connexion au serveur impossible", VerticalAlignment = VerticalAlignment.Bottom, Margin = new Thickness(5, 0, 0, 6), Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 221, 68, 68)) };
            }
        }

        private void StartConference(object sender, RoutedEventArgs e)
        {
            SoftPhone.Instance.StartConference();
        }

        private void StopConference(object sender, RoutedEventArgs e)
        {
            SoftPhone.Instance.StopConference();
        }



    }
}
