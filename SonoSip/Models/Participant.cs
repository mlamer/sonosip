﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SonoSip.Models
{
    class Participant
    {
        public Participant() { }
        public Participant(string code, string name, string phoneNumber)
        {
            this.Code = code;
            this.Name = name;
            this.PhoneNumber = phoneNumber;
        }
        public string Code { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
    }
}
